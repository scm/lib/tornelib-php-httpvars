<?php

/**
 * @version 1.0.0
 */
namespace TorneLIB\Module;

/**
 * Class HttpVars HTTP request parser
 *
 * @package TorneLIB\Module
 */
class HttpVars
{
    /**
     * @var string Endpoint request variable.
     */
    private $requestVariable;

    /**
     * @var string Initial request container.
     */
    private $request;

    /**
     * @var array Our arguments list.
     */
    private $args;

    /**
     * @var string Request method (GET, POST, DELETE, PUT, etc).
     */
    private $method;

    /**
     * @var string Verb used after endpoint.
     */
    private $verb;

    /**
     * @var string Endpoint name.
     */
    private $endpoint;

    /**
     * @var bool Use a verb or not.
     */
    private $noverb;

    /**
     * @var bool Always run lowercase controls regardless of stored arguments.
     */
    private $alwaysLowerCase = false;
    /**
     * @var bool If true, we will never throw exceptions on usage.
     */
    private $silentlyFailOnGet = false;

    /**
     * HttpVars constructor.
     *
     * @param bool $onInit Run initializer on constructor level.
     * @param string $requestVariable On endpoint mode, use this as request variable.
     * @param int $shiftFirst
     */
    public function __construct($onInit = true, $requestVariable = '', $shiftFirst = 0)
    {
        if ($onInit) {
            $this->getHttpVars($requestVariable, $shiftFirst);
        }
    }

    /**
     * Enable ability to skip exceptions. If enabled, everything that is not existing will return null.
     *
     * @param bool $enable
     * @since 1.0.0
     */
    public function setSilentFail($enable = true)
    {
        $this->silentlyFailOnGet = $enable;
    }

    /**
     * Pair array. Converts a request to keyed array list.
     *
     * @param array $arrayArgs
     * @param bool $valuesOnly
     * @return array
     * @since 1.0.0
     */
    private function array_pair($arrayArgs = array(), $valuesOnly = false)
    {
        $pairedArray = array();
        for ($keyCount = 0; $keyCount < count($arrayArgs); $keyCount = $keyCount + 2) {
            /**
             * Silently suppress things that does not exist
             */
            if (!isset($pairedArray[$arrayArgs[$keyCount]])) {
                $pairedArray[$arrayArgs[$keyCount]] = null;
            }
            if (!isset($arrayArgs[$keyCount + 1])) {
                $arrayArgs[$keyCount + 1] = null;
            }

            /**
             * Start the pairing
             */
            $pairedArray[$arrayArgs[$keyCount]] = (!is_null($arrayArgs[$keyCount + 1]) &&
            isset($arrayArgs[$keyCount + 1]) ?
                $arrayArgs[$keyCount + 1] :
                ""
            );
        }
        return $pairedArray;
    }

    /**
     * Magic for $CLASS->getParameterKey() usage.
     *
     * @param $name
     * @param $arguments
     * @return mixed|null
     * @since 1.0.0
     */
    public function __call($name, $arguments)
    {
        $return = null;

        if (preg_match('/^get/', $name)) {
            $return = $this->{lcfirst(preg_replace('/^get/',  '', $name))};
        }

        return $return;
    }

    /**
     * Magic for requesting for parameters directly.
     *
     * @param $name
     * @return mixed|null
     * @throws \Exception
     */
    public function __get($name)
    {
        $return = null;

        $snakeCaseConvert = preg_split('/(?=[A-Z])/', $name);
        $argFinder = array(
            $name,
            implode('_', $snakeCaseConvert)
        );

        if ($this->isAlwaysLowerCase()) {
            $this->args = array_map('strtolower', $this->args);
        }

        $hasValue = false;
        foreach ($argFinder as $item) {
            if ($name == $item && isset($this->args[$item])) {
                $hasValue = true;
                $return = $this->args[$item];
                break;
            }
        }

        if (!$hasValue && !$this->isSilentlyFailOnGet()) {
            throw new \Exception('No such variable', 99);
        }

        return $return;
    }

    /**
     * Parse the request.
     *
     * @param string $requestVariable
     * @param int $shiftFirst
     * @since 1.0.0
     */
    public function getHttpVars($requestVariable = '', $shiftFirst = 0)
    {
        $this->requestVariable = $requestVariable;
        if (isset($_REQUEST[$this->requestVariable]) && !is_array($_REQUEST[$this->requestVariable])) {
            $isRequestEndpoint = true;
            $this->request = $_REQUEST[$this->requestVariable];
        } else {
            $isRequestEndpoint = false;
            $this->request = $_SERVER['REQUEST_URI'];
        }

        $argsDefault = explode('/', rtrim($this->request, '/'));

        if ($shiftFirst > 0) {
            array_shift($argsDefault);
        }
        if ($isRequestEndpoint) {
            if (isset($argsDefault[0])) {
                $this->endpoint = $argsDefault[0];
                array_shift($argsDefault);
            }
            if (isset($argsDefault[0])) {
                $this->verb = $argsDefault[0];
                array_shift($argsDefault);
            }
        } else {
            array_shift($argsDefault);
        }
        if (isset($argsDefault) && is_array($argsDefault) && count($argsDefault)) {
            $this->args = $this->array_pair($argsDefault);
        }
        /**
         * Render REQUEST, POST and GET as args, so everything can be called in the same way.
         * However, if there is a GET it overrides POST, and if there is a POST it overrides REQUEST, etc.
         */
        if (is_array($_REQUEST)) {
            foreach ($_POST as $p => $v) {
                $this->args[$p] = $v;
            }
        }
        if (is_array($_POST)) {
            foreach ($_POST as $p => $v) {
                $this->args[$p] = $v;
            }
        }
        if (is_array($_GET)) {
            foreach ($_GET as $p => $v) {
                $this->args[$p] = $v;
            }
        }
        $RawJsonTest = @json_decode(file_get_contents("php://input"), true);
        if (is_array($RawJsonTest)) {
            foreach ($RawJsonTest as $p => $v) {
                $this->args[$p] = $v;
            }
        }
        if (isset($this->args[$this->requestVariable])) {
            unset($this->args[$this->requestVariable]);
        }
        if (empty($this->verb)) {
            $this->noverb = true;
            $this->verb = rtrim($this->request, '/');
        }

        // Disabled file attachment handling.
        /*if (isset($_FILES)) {
            $this->setAttachments($_FILES);
        }*/

        $this->method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : "GET";
    }

    /**
     * @return mixed
     * @since 1.0.0
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * Check if always lowercase handler is active.
     *
     * @return bool
     * @since 1.0.0
     */
    public function isAlwaysLowerCase()
    {
        return $this->alwaysLowerCase;
    }

    /**
     * Enable forced lowercase handler.
     *
     * @param bool $alwaysLowerCase
     * @since 1.0.0
     */
    public function setAlwaysLowerCase($alwaysLowerCase)
    {
        $this->alwaysLowerCase = $alwaysLowerCase;
    }

    /**
     * Check if we can silently fail or if we should enable exceptions.
     *
     * @return bool
     * @since 1.0.0
     */
    public function isSilentlyFailOnGet()
    {
        return $this->silentlyFailOnGet;
    }
}
