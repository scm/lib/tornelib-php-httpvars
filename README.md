# HTTP GET-var parser

This is a library that extracts GET/POST/REQUEST-variables into an array together with "nice-url"-based requests.

It supports some ways to pick up data from a http request. In the same time, the script also picks up jSON-based request.

## .htaccess wordpress style

```
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /test.php [L]
</IfModule>
```

## .htaccess api endpoint with verb
```
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule (.*)$ requestfile.php?request=$1 [QSA,L,NC]

RewriteCond %{REQUEST_FILENAME} -f
RewriteRule (.*)$ requestfile.php?request=$1 [QSA,L,NC]
```

## Usage examples

```
require_once('vendor/autoload.php');
$HTTP = new TorneLIB\Module\HttpVars();
$allArguments = $HTTP->getArgs();
$value = $HTTP->getMyValue();
$magicValue = $HTTP->myValue;
```
